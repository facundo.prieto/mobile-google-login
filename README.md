# Prueba de concepto (Expo)
Login con Google y grabar datos de la cuenta en una base (firebase como ejemplo) desde react-native con Expo cli

# Para poder probarlo
- clonar este repositorio
- desde una terminar parada en la capreta del proyecto ejecutar `npm install` y `expo install`


# Dependencias instaladas para crearlo
expo init mobile-google-login  
cd mobile-google-login  

- npm install react-native-elements
- npm install react-navigation-stack
- expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view
- expo install firebase
- npm install react-navigation
- expo install expo-google-app-auth
  
expo start
