import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyDZob4U-g8KRLomBZRId2T4c4A5Zy3kTJc",
  authDomain: "mobile--login.firebaseapp.com",
  databaseURL: "https://mobile--login-default-rtdb.firebaseio.com",
  projectId: "mobile--login",
  storageBucket: "mobile--login.appspot.com",
  messagingSenderId: "276072401071",
  appId: "1:276072401071:web:7c603fa885a5d79205ec5a",
  measurementId: "G-LPY3Q196EH"
};

if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig);
}

export default firebase;