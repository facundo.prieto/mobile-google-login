import React, {Component} from 'react';
import { StyleSheet, Text, View, ActivityIndicator} from 'react-native';
import firebase from '../firebaseConfig';


export class LoadingScreen extends Component {

    componentDidMount() {
        this.checkIfLoggedIn();
    }
    
    checkIfLoggedIn = () => {
        var t = this;
        firebase.auth().onAuthStateChanged(function(user){
            console.log('Chequeo si hay un usuario logueado: ', user);
            if(user){
                //si existe sigue el flujo de la app, y se muestra un boton para desloguearse
                t.props.navigation.navigate('DashboardScreen');
            }else{
                //si no hay un usuario logueado se redirecciona al login
                t.props.navigation.navigate('LoginScreen');
            }
        })
    }

    render() {
        return ( 
            <View style={styles.container}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        );
    }
}

export default LoadingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
  