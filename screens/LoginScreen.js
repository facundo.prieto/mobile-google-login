import React, {Component} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import firebase from '../firebaseConfig';
import * as Google from 'expo-google-app-auth';

export class LoginScreen extends Component {

    //Función de comparación de usuarios provista por la documentación de Firebase
    isUserEqual = (googleUser, firebaseUser) => {
        if (firebaseUser) {
          var providerData = firebaseUser.providerData;
          for (var i = 0; i < providerData.length; i++) {
            if (providerData[i].providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
                providerData[i].uid === googleUser.getBasicProfile().getId()) {
              // We don't need to reauth the Firebase connection.
              return true;
            }
          }
        }
        return false;
    }

    //Función de logueo de la documentación de Firebase, con modificaciones para que luego de loguearse grabe el usuario en una base de datos
    onSignIn = googleUser => {
        //console.log('usuario de Google elejido >', googleUser);

        // Se registra un Observer en Firebase Auth para asegurarse que auth está inicializado
        var unsubscribe = firebase.auth().onAuthStateChanged(
            function(firebaseUser) {
                unsubscribe();

                //console.log('usuario en la sesión (primera vez null) > ', firebaseUser);

                // Verifica si el usuario de Google elegido no es el logueado
                if (!this.isUserEqual(googleUser, firebaseUser)) {
                    
                    // Crea una credencial de Firebase con el token de Google ID
                    var credential = firebase.auth.GoogleAuthProvider.credential(
                        googleUser.idToken, 
                        googleUser.accessToken
                        //googleUser.getAuthResponse().id_token  - así dice la documentación que debería hacerse, pero no funciona en expo
                    );

                    // Iniciar sesión con el usuario de Google elejido.
                    firebase.auth().signInWithCredential(credential).then(function (result){
                        //console.log('Usuario logueado con exito! info > ', result);

                        // Si es un usuario nuevo, se crea en la base de datos (para la prueba es la Realtime Database de Firebase, pero deberíamos usar la api interna)
                        if (result.additionalUserInfo.isNewUser) {
                          firebase
                            .database()
                            .ref('/users/' + result.user.uid)
                            .set({
                              gmail: result.user.email,
                              profile_picture: result.additionalUserInfo.profile.picture,
                              first_name: result.additionalUserInfo.profile.given_name,
                              last_name: result.additionalUserInfo.profile.family_name,
                              created_at: Date.now()
                            })
                            .then(function(snapshot) {
                              //console.log('Snapshot', snapshot);
                            });
                        } else {
                            // Si no es un usuario nuevo, se registra el último acceso
                            firebase
                            .database()
                            .ref('/users/' + result.user.uid)
                            .update({
                                last_logged_in: Date.now()
                            });
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                        // ...
                    });
                } else {
                    console.log('User already signed-in Firebase.');
                }
            }.bind(this)
        );
    }
        
    signInWithGoogleAsync = async() => {
        try {

            //Seleccionar o crear una cuenta de Google desde el formulario web
            const result = await Google.logInAsync({
                behavior: 'web',
                androidClientId: '276072401071-hri5cj43b113kp7q7uu0onhvq8p6lkod.apps.googleusercontent.com',
                //iosClientId: 276072401071-hri5cj43b113kp7q7uu0onhvq8p6lkod.apps.googleusercontent.com,
                scopes: ['profile', 'email'],
            });
        
            if (result.type === 'success') {
                //Iniciar sesión con esa cuenta
                this.onSignIn(result)
                return result.accessToken;
            } else {
                return { cancelled: true };
            }
        } catch (e) {
            return { error: true };
        }
    }
    
  render() {
    return (
        <View style={styles.container}>
            <Button
                title="Iniciar sesión con Google"
                //onPress={() => alert('Iniciar sesión con Google')}
                onPress={() => this.signInWithGoogleAsync()}
            />
        </View>
    );
  }
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
